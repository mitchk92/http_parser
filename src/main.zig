const std = @import("std");
const testing = std.testing;

pub const Method = enum(usize) {
    GET,
    HEAD,
    POST,
    PUT,
    DELETE,
    TRACE,
    OPTIONS,
    CONNECT,
    PATCH,
};

pub const Version = enum {
    HTTP_1_1,
};

pub const HTTP = struct {
    request: []const u8,
    method: Method,
    url: []const u8,
    version: Version,
    headers_start: usize,
    body_start: usize,
    pub fn parse(data: []const u8) !HTTP {
        const delim = "\r\n";
        var lines = std.mem.split(data, delim);
        var index: usize = 0;
        if (lines.next()) |target_line| {
            index += target_line.len + delim.len;
        } else return error.TooSmall;
        const headers_start = index;
        while (lines.next()) |field| {
            index += target_line.len + delim.len;
            if (field.len == 0) {
                return HTTP{
                    .request = data,
                    .method = method,
                    .url = url,
                    .version = version,
                    .headers_start = headers_start,
                    .body_start = index,
                };
            }
        }
    }
};

test "basic add functionality" {
    try testing.expect(add(3, 7) == 10);
}
